-- MariaDB dump 10.17  Distrib 10.4.6-MariaDB, for osx10.14 (x86_64)
--
-- Host: localhost    Database: Cookbook
-- ------------------------------------------------------
-- Server version	10.4.6-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Category`
--

DROP TABLE IF EXISTS `Category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Category` (
  `id_cat` char(3) NOT NULL,
  `category` varchar(50) NOT NULL,
  PRIMARY KEY (`id_cat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Category`
--

LOCK TABLES `Category` WRITE;
/*!40000 ALTER TABLE `Category` DISABLE KEYS */;
INSERT INTO `Category` VALUES ('ant','Antipasti'),('coc','Cocktail'),('con','Contorni'),('des','Dessert'),('pri','Primi'),('sec','Secondi');
/*!40000 ALTER TABLE `Category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Flag`
--

DROP TABLE IF EXISTS `Flag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Flag` (
  `id_flag` int(11) NOT NULL AUTO_INCREMENT,
  `flag` varchar(30) NOT NULL,
  PRIMARY KEY (`id_flag`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Flag`
--

LOCK TABLES `Flag` WRITE;
/*!40000 ALTER TABLE `Flag` DISABLE KEYS */;
INSERT INTO `Flag` VALUES (1,'Tradizionali'),(2,'Salse');
/*!40000 ALTER TABLE `Flag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Ingredient`
--

DROP TABLE IF EXISTS `Ingredient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Ingredient` (
  `id_ingr` int(11) NOT NULL AUTO_INCREMENT,
  `ingredient` varchar(50) NOT NULL,
  PRIMARY KEY (`id_ingr`),
  UNIQUE KEY `ingredient` (`ingredient`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Ingredient`
--

LOCK TABLES `Ingredient` WRITE;
/*!40000 ALTER TABLE `Ingredient` DISABLE KEYS */;
/*!40000 ALTER TABLE `Ingredient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Measure`
--

DROP TABLE IF EXISTS `Measure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Measure` (
  `id_m` int(11) NOT NULL AUTO_INCREMENT,
  `measure` varchar(6) NOT NULL,
  `full_name` varchar(15) NOT NULL,
  `std_ref` int(11) DEFAULT NULL,
  `std_proportion` float(7,3) DEFAULT NULL,
  PRIMARY KEY (`id_m`),
  UNIQUE KEY `measure` (`measure`),
  KEY `FKRMMEASURE` (`std_ref`),
  CONSTRAINT `FKRMMEASURE` FOREIGN KEY (`std_ref`) REFERENCES `Measure` (`id_m`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Measure`
--

LOCK TABLES `Measure` WRITE;
/*!40000 ALTER TABLE `Measure` DISABLE KEYS */;
INSERT INTO `Measure` VALUES (1,'gr','grammo',1,1.000),(2,'l','litro',2,1.000),(3,'pz','pezzo',3,1.000),(4,'','pezzo',3,1.000),(5,'qb','quanto basta',5,1.000),(6,'dl','decilitro',2,0.100),(7,'tsp','teaspoon',2,0.005),(8,'tasp','tablespoon',2,0.015),(9,'cc','cucchiaino',2,0.005),(10,'ml','millilitro',2,0.001),(11,'cl','centilitro',2,0.010),(12,'kg','kilogrmmo',1,1000.000),(13,'c','cucchiaio',2,0.015),(14,'pt','pinta',2,0.500),(15,'oz','oncia',2,0.030),(16,'cup','tazza',2,0.250);
/*!40000 ALTER TABLE `Measure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Recipe`
--

DROP TABLE IF EXISTS `Recipe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Recipe` (
  `id_recipe` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `id_cat` char(3) DEFAULT NULL,
  `description` longtext NOT NULL,
  `difficulty` varchar(12) DEFAULT NULL,
  `time` varchar(15) DEFAULT NULL,
  `location` tinytext DEFAULT NULL,
  `note` varchar(50) DEFAULT NULL,
  `pic` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_recipe`),
  UNIQUE KEY `name` (`name`),
  KEY `FKRCAT` (`id_cat`),
  CONSTRAINT `FKRCAT` FOREIGN KEY (`id_cat`) REFERENCES `Category` (`id_cat`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Recipe`
--

LOCK TABLES `Recipe` WRITE;
/*!40000 ALTER TABLE `Recipe` DISABLE KEYS */;
/*!40000 ALTER TABLE `Recipe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Recipe_Flag`
--

DROP TABLE IF EXISTS `Recipe_Flag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Recipe_Flag` (
  `id_recipe` int(11) NOT NULL,
  `id_flag` int(11) NOT NULL,
  PRIMARY KEY (`id_recipe`,`id_flag`),
  KEY `FKRFFLAG` (`id_flag`),
  CONSTRAINT `FKRFFLAG` FOREIGN KEY (`id_flag`) REFERENCES `Flag` (`id_flag`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FKRFRECIPE` FOREIGN KEY (`id_recipe`) REFERENCES `Recipe` (`id_recipe`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Recipe_Flag`
--

LOCK TABLES `Recipe_Flag` WRITE;
/*!40000 ALTER TABLE `Recipe_Flag` DISABLE KEYS */;
/*!40000 ALTER TABLE `Recipe_Flag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Recipe_Ingredient`
--

DROP TABLE IF EXISTS `Recipe_Ingredient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Recipe_Ingredient` (
  `id_recipe` int(11) NOT NULL,
  `id_ingr` int(11) NOT NULL,
  `measure` int(11) DEFAULT NULL,
  `quantity` float(5,2) DEFAULT NULL,
  PRIMARY KEY (`id_recipe`,`id_ingr`),
  KEY `FKRIINGR` (`id_ingr`),
  KEY `FKRIMEASURE` (`measure`),
  CONSTRAINT `FKRIINGR` FOREIGN KEY (`id_ingr`) REFERENCES `Ingredient` (`id_ingr`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FKRIMEASURE` FOREIGN KEY (`measure`) REFERENCES `Measure` (`id_m`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FKRIRECIPE` FOREIGN KEY (`id_recipe`) REFERENCES `Recipe` (`id_recipe`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Recipe_Ingredient`
--

LOCK TABLES `Recipe_Ingredient` WRITE;
/*!40000 ALTER TABLE `Recipe_Ingredient` DISABLE KEYS */;
/*!40000 ALTER TABLE `Recipe_Ingredient` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-31 18:34:17
