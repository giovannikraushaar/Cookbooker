-- Giovanni Kraushaar
-- giovanni.kraushaar@gmail.com
-- 2019-08-22


/* ===========================
            COOKBOOK
         Database Schema
  ============================ */


/* Codestyle --------------------------------------------------------------- */

/*
* Table names start with upper-case letter followed by lower-case
* Attributes are always lower-case
* Underscore_separated names, NO CamelCase, whenever possible
* Constraints names all in capital letters with no underscore
* Use singular for names whenever possible
*/


/* Create and access database ---------------------------------------------- */

CREATE SCHEMA IF NOT EXISTS Cookbook ;
USE Cookbook ;


/* Cleanup ----------------------------------------------------------------- */

DROP TABLE IF EXISTS Recipe_Flag ;
DROP TABLE IF EXISTS Recipe_Ingredient ;
DROP TABLE IF EXISTS Measure ;
DROP TABLE IF EXISTS Ingredient ;
DROP TABLE IF EXISTS Flag ;
DROP TABLE IF EXISTS Recipe ;
DROP TABLE IF EXISTS Difficuty ;
DROP TABLE IF EXISTS Category ;


/* Tables ------------------------------------------------------------------ */

CREATE TABLE Category (
  id_cat CHAR(3),
  category VARCHAR(50) NOT NULL,
  PRIMARY KEY (id_cat)
);


CREATE TABLE Recipe (
  id_recipe INT AUTO_INCREMENT,
  name VARCHAR(30) NOT NULL UNIQUE,
  id_cat CHAR(3),
  description LONGTEXT NOT NULL,
  difficulty VARCHAR(12),
  time VARCHAR(15),
  location TINYTEXT,
  note VARCHAR(50),
  pic VARCHAR(30),  -- picture name 
  PRIMARY KEY (id_recipe),
  CONSTRAINT FKRCAT
    FOREIGN KEY (id_cat) REFERENCES Category (id_cat)
      ON UPDATE CASCADE  ON DELETE RESTRICT
);


CREATE TABLE Flag (
  id_flag INT AUTO_INCREMENT,
  flag VARCHAR(30) NOT NULL,
  PRIMARY KEY (id_flag)
);


CREATE TABLE Ingredient (
  id_ingr INT AUTO_INCREMENT,
  ingredient VARCHAR(50) UNIQUE NOT NULL,
  PRIMARY KEY (id_ingr)
);


CREATE TABLE Measure (
  id_m INT AUTO_INCREMENT,
  measure VARCHAR(6) UNIQUE NOT NULL,
  full_name VARCHAR(15) NOT NULL,
  std_ref INT,
  std_proportion FLOAT(7,3),
  PRIMARY KEY (id_m),
  CONSTRAINT FKRMMEASURE
    FOREIGN KEY (std_ref) REFERENCES Measure (id_m)
      ON UPDATE CASCADE  ON DELETE CASCADE
);
/*
Standard Units are:
gr for mass
l for volume
*/


CREATE TABLE Recipe_Ingredient (
  id_recipe INT,
  id_ingr INT,
  measure INT,
  quantity FLOAT(5,2), -- only numbers, possibly transform later in fractions (eg. 1/4)
  PRIMARY KEY (id_recipe,id_ingr),
  CONSTRAINT FKRIRECIPE
    FOREIGN KEY (id_recipe) REFERENCES Recipe (id_recipe)
      ON UPDATE CASCADE  ON DELETE CASCADE,
  CONSTRAINT FKRIINGR
    FOREIGN KEY (id_ingr) REFERENCES Ingredient (id_ingr)
      ON UPDATE CASCADE  ON DELETE CASCADE,
  CONSTRAINT FKRIMEASURE
    FOREIGN KEY (measure) REFERENCES Measure (id_m)
      ON UPDATE CASCADE  ON DELETE CASCADE
);


CREATE TABLE Recipe_Flag (
  id_recipe INT,
  id_flag INT,
  PRIMARY KEY (id_recipe,id_flag),
  CONSTRAINT FKRFRECIPE
    FOREIGN KEY (id_recipe) REFERENCES Recipe (id_recipe)
      ON UPDATE CASCADE  ON DELETE CASCADE,
  CONSTRAINT FKRFFLAG
    FOREIGN KEY (id_flag) REFERENCES Flag (id_flag)
      ON UPDATE CASCADE  ON DELETE CASCADE
);
