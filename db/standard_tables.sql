USE Cookbook

INSERT INTO Measure VALUES
(1,"gr","grammo",1,1),
(2,"l","litro",2,1),
(3,"pz","pezzo",3,1),
(4,"","pezzo",3,1),
(5,"qb","quanto basta",5,1),
(6,"dl","decilitro",2,0.1),
(7,"tsp","teaspoon",2,0.005),
(8,"tasp","tablespoon",2,0.015),
(9,"cc","cucchiaino",2,0.005),
(10,"ml","millilitro",2,0.001),
(11,"cl","centilitro",2,0.01),
(12,"kg","kilogrmmo",1,1000),
(13,"c","cucchiaio",2,0.015),  -- TODO add exemption for this (should be capital C)
(14,"pt","pinta",2,0.5),
(15,"oz","oncia",2,0.03),
(16,"cup","tazza",2,0.25);

-- (,"","",,),

/*
INSERT INTO Measure(measure, full_name, std_ref, std_proportion) VALUES
("","",,);
*/

INSERT INTO Category VALUES 
("pri","Primi"),
("sec","Secondi"),
("ant","Antipasti"),
("con","Contorni"),
("des","Dessert"),
("coc","Cocktail");

INSERT INTO Flag (flag) VALUES
("Tradizionali"),
("Salse");

-- TODO : no info after & or only qb.