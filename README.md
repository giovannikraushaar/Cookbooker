App to menage a recipes database and export them to a formatted cookbook.

Runs on top of MariaDB/MySQL and R with Shiny in order to run.

By default the program creates and uses a database called `Cookbook` from user `root`. Change the beginning of the scripts and the accordingly if they need to be changed.


## Getting started

Only ****, run these commands from terminal in the project directory.
```bash
mysql -u root < db/schema.MySQL
mysql -u root < db/standard_tables.MySQL
```

You may also need to install the following packages in R.

```r
install.packages('shiny')
install.packages('rlang')
install.packages('stringr')
install.packages('DBI')
install.packages('RMariaDB')
```


## Populate the database

Use the Shiny app to insert recipes inside the database. Start it with
```r
shiny::runApp('InsertRecipeApp/app.R')
```
